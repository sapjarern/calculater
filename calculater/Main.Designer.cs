﻿namespace calculater
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Result = new System.Windows.Forms.TextBox();
            this.num1BT = new System.Windows.Forms.Button();
            this.num2BT = new System.Windows.Forms.Button();
            this.num3BT = new System.Windows.Forms.Button();
            this.devideBT = new System.Windows.Forms.Button();
            this.num4BT = new System.Windows.Forms.Button();
            this.num6BT = new System.Windows.Forms.Button();
            this.num5BT = new System.Windows.Forms.Button();
            this.timeBT = new System.Windows.Forms.Button();
            this.num7BT = new System.Windows.Forms.Button();
            this.num8BT = new System.Windows.Forms.Button();
            this.num9BT = new System.Windows.Forms.Button();
            this.minutBT = new System.Windows.Forms.Button();
            this.num0BT = new System.Windows.Forms.Button();
            this.equalBT = new System.Windows.Forms.Button();
            this.plusBT = new System.Windows.Forms.Button();
            this.dotBT = new System.Windows.Forms.Button();
            this.clearBT = new System.Windows.Forms.Button();
            this.openParentheses = new System.Windows.Forms.Button();
            this.closeParentheses = new System.Windows.Forms.Button();
            this.backspaceBT = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Result
            // 
            this.Result.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Result.Location = new System.Drawing.Point(12, 52);
            this.Result.Name = "Result";
            this.Result.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Result.Size = new System.Drawing.Size(325, 38);
            this.Result.TabIndex = 0;
            this.Result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Result.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DisableKey);
            // 
            // num1BT
            // 
            this.num1BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num1BT.Location = new System.Drawing.Point(12, 97);
            this.num1BT.Name = "num1BT";
            this.num1BT.Size = new System.Drawing.Size(50, 50);
            this.num1BT.TabIndex = 1;
            this.num1BT.Text = "1";
            this.num1BT.UseVisualStyleBackColor = true;
            this.num1BT.Click += new System.EventHandler(this.num1BT_Click);
            // 
            // num2BT
            // 
            this.num2BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num2BT.Location = new System.Drawing.Point(68, 96);
            this.num2BT.Name = "num2BT";
            this.num2BT.Size = new System.Drawing.Size(50, 50);
            this.num2BT.TabIndex = 1;
            this.num2BT.Text = "2";
            this.num2BT.UseVisualStyleBackColor = true;
            this.num2BT.Click += new System.EventHandler(this.num2BT_Click);
            // 
            // num3BT
            // 
            this.num3BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num3BT.Location = new System.Drawing.Point(124, 96);
            this.num3BT.Name = "num3BT";
            this.num3BT.Size = new System.Drawing.Size(50, 50);
            this.num3BT.TabIndex = 1;
            this.num3BT.Text = "3";
            this.num3BT.UseVisualStyleBackColor = true;
            this.num3BT.Click += new System.EventHandler(this.num3BT_Click);
            // 
            // devideBT
            // 
            this.devideBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devideBT.Location = new System.Drawing.Point(180, 96);
            this.devideBT.Name = "devideBT";
            this.devideBT.Size = new System.Drawing.Size(50, 50);
            this.devideBT.TabIndex = 1;
            this.devideBT.Text = "/";
            this.devideBT.UseVisualStyleBackColor = true;
            this.devideBT.Click += new System.EventHandler(this.devideBT_Click);
            // 
            // num4BT
            // 
            this.num4BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num4BT.Location = new System.Drawing.Point(12, 153);
            this.num4BT.Name = "num4BT";
            this.num4BT.Size = new System.Drawing.Size(50, 50);
            this.num4BT.TabIndex = 1;
            this.num4BT.Text = "4";
            this.num4BT.UseVisualStyleBackColor = true;
            this.num4BT.Click += new System.EventHandler(this.num4BT_Click);
            // 
            // num6BT
            // 
            this.num6BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num6BT.Location = new System.Drawing.Point(124, 154);
            this.num6BT.Name = "num6BT";
            this.num6BT.Size = new System.Drawing.Size(50, 50);
            this.num6BT.TabIndex = 1;
            this.num6BT.Text = "6";
            this.num6BT.UseVisualStyleBackColor = true;
            this.num6BT.Click += new System.EventHandler(this.num6BT_Click);
            // 
            // num5BT
            // 
            this.num5BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num5BT.Location = new System.Drawing.Point(68, 154);
            this.num5BT.Name = "num5BT";
            this.num5BT.Size = new System.Drawing.Size(50, 50);
            this.num5BT.TabIndex = 1;
            this.num5BT.Text = "5";
            this.num5BT.UseVisualStyleBackColor = true;
            this.num5BT.Click += new System.EventHandler(this.num5BT_Click);
            // 
            // timeBT
            // 
            this.timeBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeBT.Location = new System.Drawing.Point(180, 153);
            this.timeBT.Name = "timeBT";
            this.timeBT.Size = new System.Drawing.Size(50, 50);
            this.timeBT.TabIndex = 1;
            this.timeBT.Text = "*";
            this.timeBT.UseVisualStyleBackColor = true;
            this.timeBT.Click += new System.EventHandler(this.timeBT_Click);
            // 
            // num7BT
            // 
            this.num7BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num7BT.Location = new System.Drawing.Point(12, 209);
            this.num7BT.Name = "num7BT";
            this.num7BT.Size = new System.Drawing.Size(50, 50);
            this.num7BT.TabIndex = 1;
            this.num7BT.Text = "7";
            this.num7BT.UseVisualStyleBackColor = true;
            this.num7BT.Click += new System.EventHandler(this.num7BT_Click);
            // 
            // num8BT
            // 
            this.num8BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num8BT.Location = new System.Drawing.Point(68, 208);
            this.num8BT.Name = "num8BT";
            this.num8BT.Size = new System.Drawing.Size(50, 50);
            this.num8BT.TabIndex = 1;
            this.num8BT.Text = "8";
            this.num8BT.UseVisualStyleBackColor = true;
            this.num8BT.Click += new System.EventHandler(this.num8BT_Click);
            // 
            // num9BT
            // 
            this.num9BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num9BT.Location = new System.Drawing.Point(124, 208);
            this.num9BT.Name = "num9BT";
            this.num9BT.Size = new System.Drawing.Size(50, 50);
            this.num9BT.TabIndex = 1;
            this.num9BT.Text = "9";
            this.num9BT.UseVisualStyleBackColor = true;
            this.num9BT.Click += new System.EventHandler(this.num9BT_Click);
            // 
            // minutBT
            // 
            this.minutBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minutBT.Location = new System.Drawing.Point(180, 208);
            this.minutBT.Name = "minutBT";
            this.minutBT.Size = new System.Drawing.Size(50, 50);
            this.minutBT.TabIndex = 1;
            this.minutBT.Text = "-";
            this.minutBT.UseVisualStyleBackColor = true;
            this.minutBT.Click += new System.EventHandler(this.minutBT_Click);
            // 
            // num0BT
            // 
            this.num0BT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num0BT.Location = new System.Drawing.Point(12, 265);
            this.num0BT.Name = "num0BT";
            this.num0BT.Size = new System.Drawing.Size(50, 50);
            this.num0BT.TabIndex = 1;
            this.num0BT.Text = "0";
            this.num0BT.UseVisualStyleBackColor = true;
            this.num0BT.Click += new System.EventHandler(this.num0BT_Click);
            // 
            // equalBT
            // 
            this.equalBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalBT.Location = new System.Drawing.Point(124, 266);
            this.equalBT.Name = "equalBT";
            this.equalBT.Size = new System.Drawing.Size(50, 50);
            this.equalBT.TabIndex = 1;
            this.equalBT.Text = "=";
            this.equalBT.UseVisualStyleBackColor = true;
            this.equalBT.Click += new System.EventHandler(this.equalBT_Click);
            // 
            // plusBT
            // 
            this.plusBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusBT.Location = new System.Drawing.Point(180, 265);
            this.plusBT.Name = "plusBT";
            this.plusBT.Size = new System.Drawing.Size(50, 50);
            this.plusBT.TabIndex = 1;
            this.plusBT.Text = "+";
            this.plusBT.UseVisualStyleBackColor = true;
            this.plusBT.Click += new System.EventHandler(this.plusBT_Click);
            // 
            // dotBT
            // 
            this.dotBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dotBT.Location = new System.Drawing.Point(68, 266);
            this.dotBT.Name = "dotBT";
            this.dotBT.Size = new System.Drawing.Size(50, 50);
            this.dotBT.TabIndex = 1;
            this.dotBT.Text = ".";
            this.dotBT.UseVisualStyleBackColor = true;
            this.dotBT.Click += new System.EventHandler(this.dotBT_Click);
            // 
            // clearBT
            // 
            this.clearBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearBT.Location = new System.Drawing.Point(237, 97);
            this.clearBT.Name = "clearBT";
            this.clearBT.Size = new System.Drawing.Size(100, 50);
            this.clearBT.TabIndex = 2;
            this.clearBT.Text = "C";
            this.clearBT.UseVisualStyleBackColor = true;
            this.clearBT.Click += new System.EventHandler(this.clearBT_Click);
            // 
            // openParentheses
            // 
            this.openParentheses.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openParentheses.Location = new System.Drawing.Point(237, 153);
            this.openParentheses.Name = "openParentheses";
            this.openParentheses.Size = new System.Drawing.Size(100, 50);
            this.openParentheses.TabIndex = 2;
            this.openParentheses.Text = "(";
            this.openParentheses.UseVisualStyleBackColor = true;
            this.openParentheses.Click += new System.EventHandler(this.openParentheses_Click);
            // 
            // closeParentheses
            // 
            this.closeParentheses.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeParentheses.Location = new System.Drawing.Point(237, 209);
            this.closeParentheses.Name = "closeParentheses";
            this.closeParentheses.Size = new System.Drawing.Size(100, 50);
            this.closeParentheses.TabIndex = 2;
            this.closeParentheses.Text = ")";
            this.closeParentheses.UseVisualStyleBackColor = true;
            this.closeParentheses.Click += new System.EventHandler(this.closeParentheses_Click);
            // 
            // backspaceBT
            // 
            this.backspaceBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backspaceBT.Location = new System.Drawing.Point(236, 265);
            this.backspaceBT.Name = "backspaceBT";
            this.backspaceBT.Size = new System.Drawing.Size(100, 50);
            this.backspaceBT.TabIndex = 2;
            this.backspaceBT.Text = "<=";
            this.backspaceBT.UseVisualStyleBackColor = true;
            this.backspaceBT.Click += new System.EventHandler(this.backspaceBT_Click);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 337);
            this.Controls.Add(this.backspaceBT);
            this.Controls.Add(this.closeParentheses);
            this.Controls.Add(this.openParentheses);
            this.Controls.Add(this.clearBT);
            this.Controls.Add(this.dotBT);
            this.Controls.Add(this.num5BT);
            this.Controls.Add(this.plusBT);
            this.Controls.Add(this.timeBT);
            this.Controls.Add(this.equalBT);
            this.Controls.Add(this.num0BT);
            this.Controls.Add(this.num6BT);
            this.Controls.Add(this.minutBT);
            this.Controls.Add(this.num4BT);
            this.Controls.Add(this.num9BT);
            this.Controls.Add(this.devideBT);
            this.Controls.Add(this.num8BT);
            this.Controls.Add(this.num3BT);
            this.Controls.Add(this.num7BT);
            this.Controls.Add(this.num2BT);
            this.Controls.Add(this.num1BT);
            this.Controls.Add(this.Result);
            this.Name = "Calculator";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Result;
        private System.Windows.Forms.Button num1BT;
        private System.Windows.Forms.Button num2BT;
        private System.Windows.Forms.Button num3BT;
        private System.Windows.Forms.Button devideBT;
        private System.Windows.Forms.Button num4BT;
        private System.Windows.Forms.Button num6BT;
        private System.Windows.Forms.Button num5BT;
        private System.Windows.Forms.Button timeBT;
        private System.Windows.Forms.Button num7BT;
        private System.Windows.Forms.Button num8BT;
        private System.Windows.Forms.Button num9BT;
        private System.Windows.Forms.Button minutBT;
        private System.Windows.Forms.Button num0BT;
        private System.Windows.Forms.Button equalBT;
        private System.Windows.Forms.Button plusBT;
        private System.Windows.Forms.Button dotBT;
        private System.Windows.Forms.Button clearBT;
        private System.Windows.Forms.Button openParentheses;
        private System.Windows.Forms.Button closeParentheses;
        private System.Windows.Forms.Button backspaceBT;
    }
}

