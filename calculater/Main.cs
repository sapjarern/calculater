﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculater
{
    public partial class Calculator : Form
    {
        private bool opStatus = true;
        private bool dotStatus = false;
        private bool closedParentheses = false;
        private int countParentheses = 0;
        private List<int> indexop = new List<int>();
        private List<string> infix = new List<string>();
        private Stack<string> postfix = new Stack<string>();

        public Calculator()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            opStatus = true;
            dotStatus = false;
            closedParentheses = false;
            countParentheses = 0;
            Result.Text = "";
        }

        private void PutToDisplay(char num)
        {
            Result.AppendText(num.ToString());
            Result.Focus();
        }

        private int Checkop(string op)
        {
            switch (op)
            {
                case "+":
                case "-":
                    return 1;
                case "*":
                case "/":
                    return 2;
                case "^":
                    return 3;
                default:
                    return -1;
            }
        }

        private void StringToInfix(string str,ref List<string> refInfix)
        {
            refInfix.Clear();
            string tmp = "";
            for (var i = 0; i < str.Length; i++)
            {
                if (("()*/+-".Contains(str[i])))
                {
                    //indexop.Add(i);
                    if (tmp != "")
                    {
                        refInfix.Add(tmp);
                        tmp = "";
                    }
                    refInfix.Add(str[i].ToString());
                }
                else
                {
                    tmp += str[i].ToString();
                }
            }
            refInfix.Add(tmp);
        }

        private void InfixToPostfix(List<string> Linfix,ref Stack<string> refPostfix)
        {
            refPostfix.Clear();
            Stack<string> opStack = new Stack<string>();
            Stack<string> numStack = new Stack<string>();
            foreach(var infi in Linfix)
            {
                switch (infi)
                {
                    case "(":
                        opStack.Push(infi);
                        break;
                    case ")":
                        while (opStack.Count > 0)
                        {
                            if(opStack.Peek() != "(")
                            {
                                var tmp = opStack.Pop();
                                refPostfix.Push(tmp);
                            }
                            else
                            {
                                opStack.Pop();
                            }
                            
                        }
                        break;
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                        while (opStack.Count > 0 && Checkop(infi) <= Checkop(opStack.Peek()))
                        {
                            var tmp = opStack.Pop();
                            refPostfix.Push(tmp);
                        }
                        opStack.Push(infi);
                        break;
                    default:
                        refPostfix.Push(infi);
                        break;
                }
            }
            while (opStack.Count > 0)
            {
                var tmp = opStack.Pop();
                refPostfix.Push(tmp);
            }
            foreach (var item in refPostfix.Reverse())
            {
                Console.Write(item);
            }
        }

        private void CalculatePostfix(Stack<string> Spostfix)
        {
            Stack<string> total = new Stack<string>();
            double num1;
            double num2;
            double result = 0.0;
            foreach(var item in Spostfix.Reverse())
            {
                if(Spostfix.Count > 2)
                {
                    switch (item)
                    {
                        case "+":
                            num2 = Convert.ToDouble(total.Pop());
                            num1 = Convert.ToDouble(total.Pop());
                            result = num1 + num2;
                            total.Push(result.ToString());
                            break;
                        case "-":
                            num2 = Convert.ToDouble(total.Pop());
                            num1 = Convert.ToDouble(total.Pop());
                            result = num1 - num2;
                            total.Push(result.ToString());
                            break;
                        case "*":
                            num2 = Convert.ToDouble(total.Pop());
                            num1 = Convert.ToDouble(total.Pop());
                            result = num1 * num2;
                            total.Push(result.ToString());
                            break;
                        case "/":
                            num2 = Convert.ToDouble(total.Pop());
                            num1 = Convert.ToDouble(total.Pop());
                            result = num1 / num2;
                            total.Push(result.ToString());
                            break;
                        default:
                            total.Push(item);
                            break;
                    }
                }
                else
                {
                    result = Convert.ToDouble(Spostfix.Pop());
                }
                
            }
            Console.WriteLine(result);
            Result.Text = result.ToString();
        }

        private void num1BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('1');
                opStatus = false;
            }
        }

        private void num2BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('2');
                opStatus = false;
            }
        }

        private void num3BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('3');
                opStatus = false;
            }
        }
        private void num4BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('4');
                opStatus = false;
            }
        }

        private void num5BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('5');
                opStatus = false;
            }
        }

        private void num6BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('6');
                opStatus = false;
            }
        }

        private void num7BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('7');
                opStatus = false;
            }
        }

        private void num8BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('8');
                opStatus = false;
            }
        }

        private void num9BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('9');
                opStatus = false;
            }
        }

        private void num0BT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                PutToDisplay('0');
                opStatus = false;
            }
        }

        private void dotBT_Click(object sender, EventArgs e)
        {
            if (!closedParentheses)
            {
                if (!dotStatus)
                {
                    if (opStatus)
                    {
                        PutToDisplay('0');
                        PutToDisplay('.');
                        dotStatus = true;
                    }
                    else
                    {
                        PutToDisplay('.');
                        dotStatus = true;
                    }
                }
            }
        }

        private void equalBT_Click(object sender, EventArgs e)
        {
            string equation = Result.Text;
            Console.WriteLine();
            StringToInfix(equation, ref infix);
            Console.WriteLine();
            InfixToPostfix(infix, ref postfix);
            Console.WriteLine();
            CalculatePostfix(postfix);
            Console.WriteLine();
        }

        private void plusBT_Click(object sender, EventArgs e)
        {
            if (!opStatus)
            {
                PutToDisplay('+');
                opStatus = true;
                dotStatus = false;
                closedParentheses = false;
            }
        }

        private void minutBT_Click(object sender, EventArgs e)
        {
            if (!opStatus)
            {
                PutToDisplay('-');
                opStatus = true;
                dotStatus = false;
                closedParentheses = false;
            }
        }

        private void timeBT_Click(object sender, EventArgs e)
        {
            if (!opStatus)
            {
                PutToDisplay('*');
                opStatus = true;
                dotStatus = false;
                closedParentheses = false;
            }
        }

        private void devideBT_Click(object sender, EventArgs e)
        {
            if (!opStatus)
            {
                PutToDisplay('/');
                opStatus = true;
                dotStatus = false;
                closedParentheses = false;
            }
        }

        private void openParentheses_Click(object sender, EventArgs e)
        {
            if (opStatus)
            {
                PutToDisplay('(');
                countParentheses++;
                closedParentheses = false;
            }
        }

        private void closeParentheses_Click(object sender, EventArgs e)
        {
            if (!opStatus)
            {
                if (countParentheses > 0)
                {
                    PutToDisplay(')');
                    countParentheses--;
                    closedParentheses = true;
                }
            }
        }

        private void clearBT_Click(object sender, EventArgs e)
        {
            init();
        }

        private void backspaceBT_Click(object sender, EventArgs e)
        {
            bool prevclosedParentheses = closedParentheses;
            bool prevopStatus = opStatus;
            bool prevdotStatus = dotStatus;
            string str = "";
            int lastindex;
            if(Result.Text.Length > 0)
            {
                str = Result.Text;
                Console.WriteLine(str);
                lastindex = str.Length - 1;
                switch (str[lastindex])
                {
                    case '(':
                        countParentheses--;
                        closedParentheses = !prevclosedParentheses;
                        break;
                    case ')':
                        countParentheses++;
                        closedParentheses = !prevclosedParentheses;
                        break;
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                        opStatus = !prevopStatus;
                        dotStatus = !prevdotStatus;
                        closedParentheses = !prevclosedParentheses;
                        break;
                    case '.':
                        dotStatus = !prevdotStatus;
                        break;
                }
                Result.Text = str.Substring(0, str.Length - 1);
            }
            
        }

        private void DisableKey(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
